package com.soft.employees.entity;

public class ReportTo {

    private long Id;
    private String name;

    public ReportTo(){}

    public ReportTo(long Id,String name){
        this.Id = Id;
        this.name = name;
    }

    public Long getId() {
        return Id;
    }

    public void setId(long Id) {
        this.Id = Id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
