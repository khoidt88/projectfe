package com.soft.employees.repository;

import com.soft.employees.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee,Long> {

    Page<Employee> findEmployeesByLastNameContaining(@Param("lastName") String lastName, Pageable pageable);

    @Query(value = "from Employee t where t.lastName LIKE %:lastName% AND t.hireDate BETWEEN :startDate AND :endDate")
    Page<Employee> findEmployeesByLastNameAndHireDate(@Param("lastName") String lastName,
                                                      @Param(value="startDate")@DateTimeFormat(pattern="yyyy-MM-dd")
                                                              Date startDate,
                                                      @Param(value="endDate")@DateTimeFormat(pattern="yyyy-MM-dd") Date endDate, Pageable pageable);
    
}