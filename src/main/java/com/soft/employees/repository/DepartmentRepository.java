package com.soft.employees.repository;

import com.soft.employees.entity.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface DepartmentRepository extends PagingAndSortingRepository<Department,Long> {

    Page<Department> findAllByNameContaining(@Param("name") String name, Pageable pageable);

    Page<Department> findAllByNameContainingAndDescriptionContaining(@Param("name") String name,@Param("description") String description,Pageable pageable);


}
