package com.soft.employees.controller;

import com.soft.employees.entity.Department;
import com.soft.employees.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/department/")
public class DepartmentController {

    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentController(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @GetMapping("list")
    public String list(@PageableDefault(size = 5,sort = "id") Pageable pageable,
                       Model model)
    {
        Page<Department> page = departmentRepository.findAll(pageable);
        List<Sort.Order> sortOrders = page.getSort().stream().collect(Collectors.toList());
        if (sortOrders.size() > 0) {
            Sort.Order order = sortOrders.get(0);
            model.addAttribute("sortProperty", order.getProperty());
            model.addAttribute("sortDesc", order.getDirection() == Sort.Direction.DESC);
        }
        model.addAttribute("page",page);
        return "list-department";
    }

    @GetMapping("search")
    public String searchlastNameAndHireDate(@RequestParam("name") String name,
                                            @RequestParam("description") String description,
                                            Model model,
                                            @PageableDefault(size = 5) Pageable pageable) {
        if (StringUtils.isEmpty(name) || (StringUtils.isEmpty(description))) {
            return "redirect:/department/list";
        }
        if(StringUtils.isEmpty(description)){
            Page<Department> page = departmentRepository.findAllByNameContaining(name,pageable);
            model.addAttribute("name",name);
            model.addAttribute("description",description);
            model.addAttribute("page",page);
            return "list-department";
        }
            Page<Department> page = departmentRepository.findAllByNameContainingAndDescriptionContaining(name,description,pageable);
            model.addAttribute("name",name);
            model.addAttribute("description",description);
            model.addAttribute("page",page);
            return "list-department";
    }


}
