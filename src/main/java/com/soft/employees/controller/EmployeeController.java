package com.soft.employees.controller;

import com.soft.employees.entity.Employee;
import com.soft.employees.entity.ReportTo;
import com.soft.employees.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/employees/")
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    String rootPath = System.getProperty("user.dir");

    @Autowired
    public EmployeeController(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @GetMapping("new")
    public String showNewEmployeePage(Model model) {
        Employee employee = new Employee();
        List<ReportTo> reportLists = new ArrayList<>();
        reportLists.add(new ReportTo(1,"Nguyen Tuan"));
        reportLists.add(new ReportTo(2,"Kien Nguyen"));
        reportLists.add(new ReportTo(3,"Sim Anh"));
        reportLists.add(new ReportTo(4,"Linh Tran"));
        reportLists.add(new ReportTo(5,"Linh Vu"));

        model.addAttribute("reportLists",reportLists);
        model.addAttribute("employee",employee);

        return "add-employee";
    }

    @GetMapping("search")
    public String searchlastNameAndHireDate(@RequestParam("lastName") String lastName,
                                            @RequestParam(value="startDate")@DateTimeFormat(pattern="yyyy-MM-dd") Date startDate,
                                            @RequestParam(value="endDate")@DateTimeFormat(pattern="yyyy-MM-dd") Date endDate,
                                            Model model,
                                            @PageableDefault(size = 5) Pageable pageable) {
        if (StringUtils.isEmpty(lastName) || (StringUtils.isEmpty(lastName) && startDate == null && endDate == null)) {
            return "redirect:/employees/list";
        }
        if(startDate == null && endDate == null || startDate == null || endDate == null){
            Page<Employee> page = employeeRepository.findEmployeesByLastNameContaining(lastName,pageable);
            model.addAttribute("lastName",lastName);
            model.addAttribute("page",page);
            return "index";
        }

        Page<Employee> page = employeeRepository.findEmployeesByLastNameAndHireDate(lastName, startDate,endDate, pageable);
        model.addAttribute("lastName",lastName);
        model.addAttribute("startDate",startDate);
        model.addAttribute("endDate",endDate);
        model.addAttribute("page", page);
        return "index";
    }

    @GetMapping("list")
    public String getEmployees(@PageableDefault(size = 5,sort = "employeeId") Pageable pageable,
                               Model model) {
        Page<Employee> page = employeeRepository.findAll(pageable);
        List<Sort.Order> sortOrders = page.getSort().stream().collect(Collectors.toList());
        if (sortOrders.size() > 0) {
            Sort.Order order = sortOrders.get(0);
            model.addAttribute("sortProperty", order.getProperty());
            model.addAttribute("sortDesc", order.getDirection() == Sort.Direction.DESC);
        }
        model.addAttribute("page",page);
        return "index";
    }

    @PostMapping("add")
    public String addEmployee(@Valid Employee employee, BindingResult result) throws IOException, SQLException {
        if(result.hasErrors()){
            return "add-employee";
        }
        employeeRepository.save(employee);
        String dir = rootPath + File.separator + "images\\";
        BufferedImage photo = ImageIO.read(new ByteArrayInputStream(employee
                .getPhotoPath().getBytes()));
        File destination = new File(dir
                + employee.getEmployeeId() + "_photo" + ".jpg");
        ImageIO.write(photo, "jpg", destination);
        return "redirect:list?q=Add employee scuess";
    }

    @GetMapping("edit/{id}")
    public String showEditEmployee(Model model,@PathVariable("id") long id){

        Employee employee = null;
        employee = employeeRepository.findById(id)
                    .orElseThrow(() -> new IllegalArgumentException("Invalid employee Id:" + id));

        List<ReportTo> reportLists = new ArrayList<>();
        reportLists.add(new ReportTo(1,"Nguyen Tuan"));
        reportLists.add(new ReportTo(2,"Kien Nguyen"));
        reportLists.add(new ReportTo(3,"Sim Anh"));
        reportLists.add(new ReportTo(4,"Linh Tran"));
        reportLists.add(new ReportTo(5,"Linh Vu"));

        model.addAttribute("reportLists",reportLists);
        model.addAttribute("employee", employee);
        return "update-employee";
    }

    @PostMapping("update/{id}")
    public String updateEmployee(@PathVariable("id") long id, @Valid Employee employee, BindingResult result,
                                 Model model,@PageableDefault(size = 5,sort = "employeeId") Pageable pageable) throws IOException {

        if (result.hasErrors()) {
            employee.setEmployeeId(id);
            return "update-employee";
        }
        employee.setEmployeeId(id);

        String dir = rootPath + File.separator + "images\\";
        BufferedImage photo = ImageIO.read(new ByteArrayInputStream(employee
                .getPhotoPath().getBytes()));
        File destination = new File(dir
                + employee.getEmployeeId() + "_photo" + ".jpg");
        ImageIO.write(photo, "jpg", destination);

        employeeRepository.save(employee);
        Page<Employee> page = employeeRepository.findAll(pageable);
        List<Sort.Order> sortOrders = page.getSort().stream().collect(Collectors.toList());
        if (sortOrders.size() > 0) {
            Sort.Order order = sortOrders.get(0);
            model.addAttribute("sortProperty", order.getProperty());
            model.addAttribute("sortDesc", order.getDirection() == Sort.Direction.DESC);
        }

        model.addAttribute("page", page);
        return "index";
    }

    @GetMapping("delete/{id}")
    public String deleteEmployee(@PathVariable("id") long id, Model model,@PageableDefault(size = 5,sort = "employeeId") Pageable pageable) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid employee Id:" + id));
        employeeRepository.delete(employee);
        //model.addAttribute("page", employeeRepository.findAll());
        Page<Employee> page = employeeRepository.findAll(pageable);
        List<Sort.Order> sortOrders = page.getSort().stream().collect(Collectors.toList());
        if (sortOrders.size() > 0) {
            Sort.Order order = sortOrders.get(0);
            model.addAttribute("sortProperty", order.getProperty());
            model.addAttribute("sortDesc", order.getDirection() == Sort.Direction.DESC);
        }
        model.addAttribute("page",page);
        return "index";
    }

}
