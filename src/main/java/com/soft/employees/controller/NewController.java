package com.soft.employees.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/new/")
public class NewController {

    @GetMapping("list")
    public String list(Model model){
        return "list-new";
    }

}
