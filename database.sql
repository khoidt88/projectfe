USE [Chat]
GO
/****** Object:  Table [dbo].[employees]    Script Date: 6/20/2020 10:18:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[employees](
	[employee_id] [int] IDENTITY(1,1) NOT NULL,
	[last_name] [nvarchar](250) NULL,
	[first_name] [nvarchar](250) NULL,
	[title] [nvarchar](250) NULL,
	[title_of_courtesy] [nvarchar](250) NULL,
	[birth_date] [datetime] NULL,
	[hire_date] [datetime] NULL,
	[address] [nvarchar](250) NULL,
	[city] [nvarchar](250) NULL,
	[region] [nvarchar](250) NULL,
	[postal_code] [nvarchar](250) NULL,
	[country] [nvarchar](250) NULL,
	[home_phone] [nvarchar](250) NULL,
	[extension] [nvarchar](250) NULL,
	[photo] [nvarchar](250) NULL,
	[notes] [ntext] NULL,
	[reports_to] [int] NULL,
	[photo_path] [nvarchar](255) NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[employee_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[employees] ON 

INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (1, N'Khoi', N'Tuan', N'developer', N'developer', CAST(N'2020-06-09 00:00:00.000' AS DateTime), CAST(N'2020-06-10 00:00:00.000' AS DateTime), N'sss', N'ss', N'region 1', N'postal code 1', N'Việt Nam 1', N'0914862929 1', N'extension  1', N'ss', N'notes 2', 1, N'11')
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (2, N'Anh', N'Pham Sim', N'PM', N'PM', CAST(N'2020-06-01 00:00:00.000' AS DateTime), CAST(N'2020-06-10 00:00:00.000' AS DateTime), N'sss 11', N'ss', N'region 2', N'postal code 12', N'Việt Nam 12', N'0914862929 1', N'extension  12', N'ss12', N'notes 23', 2, N'112')
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (3, N'Tuan', N'Nguyen Quoc', N'PM', N'PM', CAST(N'2020-06-02 00:00:00.000' AS DateTime), CAST(N'2020-06-09 00:00:00.000' AS DateTime), N'sss', N'ss', N'region 1', N'postal code 1', N'Việt Nam 1', N'0914862929 1', N'extension  11', N'ss12', N'notes 22', 3, N'photo path31')
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (16, N'Linh', N'Nguyen Quoc', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3, NULL)
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (17, N'Truong', N'Anh ', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL)
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (18, N'Phuong', N'Cong', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL)
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (19, N'Quyet', N'Nguyen Quyet', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6, NULL)
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (20, N'Seo', N'Park Han', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7, NULL)
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (21, N'Jun', N'Lee Min', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 8, NULL)
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (22, N'Min', N'Ha', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9, NULL)
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (23, N'Sunny', N'Jun', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 10, NULL)
INSERT [dbo].[employees] ([employee_id], [last_name], [first_name], [title], [title_of_courtesy], [birth_date], [hire_date], [address], [city], [region], [postal_code], [country], [home_phone], [extension], [photo], [notes], [reports_to], [photo_path]) VALUES (24, N'Lee', N'Hoo', N'PM', N'PM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 11, NULL)
SET IDENTITY_INSERT [dbo].[employees] OFF
